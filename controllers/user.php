<?php

/**
 *	User.php
 *
 *	Defines the User related dispatch / route implementations to be used
 *	by the application. 
 *
 *	@package	BlueIgnis
 *	@component	Controllers
 */

/**
 *	Entry point of the application
 *
 *	@method	GET
 *	@route	/
 */
function blueignis() {
	if(isset($_SESSION['userauth'])) {
		return redirect('/campaign/create');
	} else {
		// Login the user
		return redirect('/login');
	}
}

/**
 *	Render the Login for the User
 *
 *	@method	GET
 *	@route	/login
 */
function login() {
	return render('login.html.php');
}

/**
 *	Authenticate the user and redirect accordingly
 *
 *	@method	POST
 *	@route	/authenticate
 */
function authenticate() {
	global $db;
	
	$username = $_POST['username'];
	$password = md5($_POST['password']);    // all passwords are encrypted

	$user = $db->select("user", "email = :username and password = :password", array(":username" => $username, ":password" => $password));
	if(count($user) > 0) {
		// Seems like user exist auth 'em
		$_SESSION['authId'] = $user[0]['id'];
		$_SESSION['userauth'] = $_SESSION['authId'];
		
		return blueignis();
	} else {
		// Sorry invalid credentials so pass the info along
		$_SESSION['invalid_login'] = "Sorry invalid username / password";
		return login();
	}
}

/**
 *	Logout the current user
 *
 *	@method	GET
 *	@route	/logout
 */
function logout() {
	unset($_SESSION['userauth']);
	unset($_SESSION['authId']);
	
	return redirect('/login');
} 

/**
 *	Render the Campaign Specific Dashboard
 *
 *	@method	GET
 *	@route	/campaign/:id/realtime/twitter
 */
function campaignRealtimeTwitterDashboard($id) {
	if(!isset($_SESSION['userauth'])) {	// Check if the user authenticated before rendering this page
		// Login the user
		return redirect('/login');
	}

	global $db;

	$campaign = $db->select("campaign", "id = :id", array(":id" => $id));
	$campaign = $campaign[0];
	
	$campaigns = $db->select("campaign", "user_id = :userid", array(":userid" => $_SESSION['authId']));

	layout('dashboard.html.php');
	// Getting the user from the session
	set('user', $_SESSION['user']);
	set('isTwitterDashboard', true);
	set('is_campaign', true);
	set('currentCampaignId', $id);
	set('campaignName', $campaign['name']);
	set('campaigns', $campaigns);
	set('authId', $_SESSION['authId']);
	set('user', $_SESSION['user']);
	set('db', $db);

	return render("twitter_dashboard.html.php");
}

/**
 *	Render the Campaign Specific Sentiment Dashboard
 *
 *	@method	GET
 *	@route	/campaign/:id/realtime/sentiment
 */
function campaignRealtimeSentimentDashboard($id) {
	if(!isset($_SESSION['userauth'])) {	// Check if the user authenticated before rendering this page
		// Login the user
		return redirect('/login');
	}

	global $db;
	
	$campaign = $db->select("campaign", "id = :id", array(":id" => $id));
	$campaign = $campaign[0];
	
	$campaigns = $db->select("campaign", "user_id = :userid", array(":userid" => $_SESSION['authId']));

	layout('dashboard.html.php');
	// Getting the user from the session
	set('user', $_SESSION['user']);
	set('isSentimentDashboard', true);
	set('is_campaign', true);
	set('currentCampaignId', $id);
	set('campaignName', $campaign['name']);
	set('campaigns', $campaigns);
	set('authId', $_SESSION['authId']);
	set('user', $_SESSION['user']);
	set('db', $db);

	return render("sentiment_dashboard.html.php");
}

/**
 *	Renders view for creating a new Campaign
 *
 *	@method	GET
 *	@route	/campaign/create
 */
function createNewCampaign() {
	if(!isset($_SESSION['userauth'])) {	// Check if the user authenticated before rendering this page
		// Login the user
		return redirect('/login');
	}

	global $db;	
	$campaigns = $db->select("campaign", "user_id = :userid", array(":userid" => $_SESSION['authId']));


	layout('dashboard.html.php');
	// Getting the user from the session
	set('user', $_SESSION['user']);
	set('currentCampaignId', -1);	// Since there is no campaign
	set('campaignName', "Create New Campaign");
	set('createCampaign', true); 
	set('campaigns', $campaigns);
	set('authId', $_SESSION['authId']);
	set('user', $_SESSION['user']);
	set('db', $db);
	
	return render("new_campaign.html.php");
}

/**
 *	Renders the Campaign dashboard
 *
 *	@method	GET
 *	@route	/campaign/:id
 */
function campaignDashboard($id) {
	if(!isset($_SESSION['userauth'])) {	// Check if the user authenticated before rendering this page
		// Login the user
		return redirect('/login');
	}

	global $db;	
	$campaign = $db->select("campaign", "id = :id", array(":id" => $id));
	$campaign = $campaign[0];
	
	$campaigns = $db->select("campaign", "user_id = :userid", array(":userid" => $_SESSION['authId']));


	layout('dashboard.html.php');
	// Getting the user from the session
	set('user', $_SESSION['user']);
	set('is_campaign', true);
	set('currentCampaignId', $id);
	set('campaignName', $campaign['name']);
	set('campaigns', $campaigns);
	set('authId', $_SESSION['authId']);
	set('user', $_SESSION['user']);
	set('db', $db);
	
	return render("campaign_dashboard.html.php");
}

/**
 *	Render Key Influencers Dashboard page 
 *
 *	@method	GET
 *	@route	/campaign/:id/report/ki
 */
function campaignReportKeyInfluencers($id) {
	if(!isset($_SESSION['userauth'])) {	// Check if the user authenticated before rendering this page
		// Login the user
		return redirect('/login');
	}

	global $db;	
	$campaign = $db->select("campaign", "id = :id", array(":id" => $id));
	$campaign = $campaign[0];
	
	$campaigns = $db->select("campaign", "user_id = :userid", array(":userid" => $_SESSION['authId']));


	layout('dashboard.html.php');
	// Getting the user from the session
	set('user', $_SESSION['user']);
	set('isKI', true);
	set('is_campaign', true);
	set('currentCampaignId', $id);
	set('campaignName', $campaign['name']);
	set('campaigns', $campaigns);
	set('authId', $_SESSION['authId']);
	set('user', $_SESSION['user']);
	set('db', $db);

	return render("keyinfluencers.html.php");
}

/**
 *	Run the Campaign after creating it
 *
 *	@method	POST
 *	@route	/campaign/create/run
 */
function runCampaign() {
	if(!isset($_SESSION['userauth'])) {	// Check if the user authenticated before rendering this page
		// Login the user
		return redirect('/login');
	}

	global $db;
	global $predis;

	$tracks = $_POST['campaign_tracks'];
	$tracks = explode(",", $tracks);
	
	print_r($tracks);

	$service = $_POST['campaign_service'];
	$name = $_POST['campaign_name'];
	
	$campaign = array(
		"name" => $name, // TODO: May be sometime we should check for possible duplicates
		"created_at" => date("Y-m-d H:i:s"),
		"user_id" => $_SESSION['authId'],
	);
	
	$db->insert("campaign", $campaign);
	$newCid = $db->lastInsertId();	// Get the Id value of the Campaign value that was inserted last
	
	foreach($tracks as $track) {
		$t = array(
			"name" => strtolower(trim($track)),	// Always rename the words to lower string. 
			"is_archived" => false,
			"campaign_id" => $newCid,
		);
		
		// Add the new track to the redis for processing within the next 1 -2 minute(s)
		$predis->lpush('blueignis:new:campaign:tracks', $t['name']);
		$db->insert("tracks", $t);
	}

	// Starting the Campaign Threaded Deamon Process
	// TODO This does seem to be of any good as of now
	// exec("php /home/blueignis/blueignis/spout/service/ThreadedCampaignQueueWorker.php > /home/blueignis/blueignis/spot/service/campaign-" . $newCid . ".log 2>&1 &"); 
	$predis->lpush('blueignis:new:campaign', $newCid);	// Store the latest Campaign Id which will be started by the Cron Master
	
	flash('created_new', true);
	flash('created_status', 'success');
	flash('created_log', 'Your Campaign has been queued and will start momentarily.');
	
	// Now redirect the user to the Campaign Dashboard
	return redirect(url_for("/campaign/$newCid"));
}


/**
 *	Delete a Campaign completely along with its related data
 *
 *	@method	GET
 *	@route	^/campaign/(\d+)/delete
 */
function deleteCampaign($id) {
	global $db;
	
	$db->run("delete from tweet_mood where tweets_id IN (select id from tweets where campaign_id = :cid); delete from tweets where campaign_id = :cid; delete from tracks where campaign_id = :cid; delete from campaign where id = :cid;", array(":cid" => $id));
	
	return redirect(url_for('/campaign/create'));
}


