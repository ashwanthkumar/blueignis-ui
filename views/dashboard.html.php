<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>BlueIgnis - <?php echo $campaignName; ?></title>
    <meta name="description" content="BlueIgnis - Social Analytical Engine">
    <meta name="author" content="Ashwanth Kumar <ashwanthkumar@googlemail.com>">
	
    <!-- Le styles -->
    <link href="<?php echo url_for('/static/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
    </style>
    <link href="<?php echo url_for('/static/css/bootstrap-responsive.css'); ?>" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">BlueIgnis</a>
          <div class="nav-collapse">
            <p class="navbar-text pull-right"><a href="<?php echo url_for('/user/profile'); ?>"><?php echo $user['name']; ?></a> | <a href="<?php echo url_for('/logout'); ?>">Logout </a></p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">

              <li class="nav-header">Campaigns</li>
              <?php
              		// $currentCampaignId = 1; // We should be passing this parameter to the layout using set('currentCampaignId', $campaignIdValue); 
	              	foreach($campaigns as $c) {
              ?>
              	<li <?php if($c['id'] == $currentCampaignId) echo "class='active'"; ?>><a href="<?php echo url_for('/campaign/' . $c['id']); ?>" ><?php echo $c['name']; ?></a></li>
              <?php
              	}
              ?>
              <li class="divider"></li>
              <li <?php if(isset($createCampaign)) echo "class='active'"; ?>><a href="<?php echo url_for('/campaign/create'); ?>" title="Create a new Campaign">Create New Campaign</a></li>
              
							
							<?php 
								if(isset($is_campaign)) {
									// Show the Campaign tools only if we are viewing a campaign
							?>
              <li class="divider"></li>
              <li class="nav-header">Campaign Tools</li>
              <li <?php if(isset($isTwitterDashboard)) echo "class='active'"; ?>><a href="<?php echo url_for('/campaign/' . $currentCampaignId . '/realtime/twitter'); ?>" title="Real time Twitter Dashboard" >Twitter Dashboard</a></li>
              <!-- <li><a href="#" title="Realtime Facebook Dashboard">Facebook Dashboard <span title="Coming Soon">#</span></a></li> -->
              <li <?php if(isset($isSentimentDashboard)) echo "class='active'"; ?>><a href="<?php echo url_for('/campaign/' . $currentCampaignId . '/realtime/sentiment'); ?>" title="Real time Sentiment Dashboard">Sentiment Dashboard</a></li>
              <li <?php if(isset($isKI)) echo "class='active'"; ?>><a href="<?php echo url_for('/campaign/' . $currentCampaignId . '/report/ki'); ?>" title="Interactive Graph of Key Influencers">Key Influencers</a></li>
              
              <li class="divider"></li>
              
              <li><a href="<?php echo url_for('/campaign/' . $currentCampaignId . '/delete'); ?>">Delete Campaign </a></li>
              <?php
              	} 
              ?>

            </ul>
          </div><!--/.well -->
        </div><!--/span-->

        <div class="span9">
          <div class="row-fluid">
          	<?php 
          		// Dashboard content goes here
          		if(isset($body)) echo $body;
          	?>
          </div><!--/row-->
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; <a href="http://ashwanthkumar.in/">Ashwanth Kumar</a> Productions, 2012</p>
      </footer>

    </div><!--/.fluid-container-->



    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo url_for('/static/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo url_for('/static/js/bootstrap.min.js'); ?>"></script>
    <script src="http://js.pusher.com/1.11/pusher.min.js"></script>
		<script type='text/javascript'>
			function linkify(text) {  
				var urlRegex =/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;  
				var usernameRegex = /(^|\s)@(\w+)/g;
				text = text.replace(urlRegex, function(url) {  
								return '<a href="' + url + '">' + url + '</a>';  
					});
				text = text.replace(usernameRegex, '$1@<a href="http://twitter.com/$2">$2</a>');
				return text;
      }
		
			// Enable pusher logging - don't include this in production
			Pusher.log = function(message) {
				// if (window.console && window.console.log) window.console.log(message);
			};

			// Flash fallback logging - don't include this in production
			WEB_SOCKET_DEBUG = true;

			var pusher = new Pusher('0346ddbdb2e3f102a781'); // Replace with your app key
			<?php 
				// Add Subscribe to Twitter Dashboard for this Campaign
				if(isset($isTwitterDashboard) && $isTwitterDashboard) {
			?>
			var twitter = pusher.subscribe('blueignis-twitter-dashboard-<?php echo $currentCampaignId; ?>');
			twitter.bind('new_tweet', function(data) {
				data = jQuery.parseJSON(data.message);
				var item = '<div class="stream-item"><div class="stream-item-inner"><div class="stream-item-icon"><img src="' + data.user_img +'"></div><div class="stream-item-title"><h3>@' + data.user_name +'</h3></div><div class="stream-item-content"><p>'+ linkify(data.tweet) + '</p></div><div class="stream-item-actions"><!-- We can add any Stream specific Actions to be performed here --></div></div></div>';
		
				$('.stream-body').prepend(item);
				
				// Keep only last 100 items on the page at any given time to have the page more responsive
				if($('.stream-item').length > 100) $('.stream-item').last().remove();				
			});
			<?php
				}
				// Add Subscription to Sentiment Dashboard for this Campaign
				if(isset($isSentimentDashboard) && $isSentimentDashboard) {
			?>
			var positive = pusher.subscribe('blueignis-sentiment-positive-dashboard-<?php echo $currentCampaignId; ?>');
			var negative = pusher.subscribe('blueignis-sentiment-negative-dashboard-<?php echo $currentCampaignId; ?>');
			positive.bind('new_tweet', function(data) {
				data = jQuery.parseJSON(data.message);
				var item = '<div class="positive-stream-item stream-item"><div class="stream-item-inner"><div class="stream-item-icon"><img src="' + data.user_img +'"></div><div class="stream-item-title"><h3>@' + data.user_name +'</h3></div><div class="stream-item-content"><p>'+ linkify(data.tweet) + '</p></div><div class="stream-item-actions-' + data.tweet_id + '"><!-- We can add any Stream specific Actions to be performed here --></div></div></div>';
		
				$('.positive-stream-body').prepend(item);
				if($('.positive-stream-item').length > 100) $('.positive-stream-item').last().remove();
			});
			
			negative.bind('new_tweet', function(data) {
				data = jQuery.parseJSON(data.message);
				var item = '<div class="negative-stream-item stream-item"><div class="stream-item-inner"><div class="stream-item-icon"><img src="' + data.user_img +'"></div><div class="stream-item-title"><h3>@' + data.user_name +'</h3></div><div class="stream-item-content"><p>'+ linkify(data.tweet) + '</p></div><div class="stream-item-actions"><!-- We can add any Stream specific Actions to be performed here --></div></div></div>';
		
				$('.negative-stream-body').prepend(item);
				if($('.negative-stream-item').length > 100) $('.negative-stream-item').last().remove();
			});
			<?php
				}
			?>
		</script>
		<?php
			if(isset($script)) echo $script;
		?>
  </body>
</html>
