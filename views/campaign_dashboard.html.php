<?php

content_for('body');

$tracks = $db->select("tracks", "campaign_id = :cid", array(":cid" => $currentCampaignId));

$t = array();

$track_list_str = "";

foreach($tracks as $track) {
	$_t = array();
	$_t['name'] = $track['name'];
	
	$_t['is_archived'] = $track['is_archived'];
	if($_t['is_archived'] == 1) $track_list_str .= "<s>" . $_t['name'] . "</s>";
	else $track_list_str .= $_t['name'] . ", ";
	
	
	$t[] = $_t;
}


if(isset($flash['created_new']) && $flash['created_status'] == "success") {
?>
<div class="alert alert-success">	<?php echo $flash['created_log']; ?> </div>
<?php
} else if(isset($flash['created_new']) && $flash['created_status'] == "error") {
?>
<div class="alert alert-error">	<?php echo $flash['created_log']; ?> </div>
<?php
}

$campaign = $db->select("campaign", "id = :cid", array(":cid" => $currentCampaignId));
?>
<div class="hero-unit">
	<h1>Dashboard</h1>
	<p>Tracks: <?php echo substr(trim($track_list_str), 0, -1); ?></p>
</div>

<div class="row-fluid">
<!--
	<h2>Twitter Stats</h2>
	<h2>Sentiment Stats on Tweets </h2>
-->

	<div class="span12">
    <div id="overall_twitter_stats" style="width: 95%; height: 500px;"></div>
    <p>
			<a class="btn" href="<?php echo url_for('/campaign/' . $currentCampaignId . '/realtime/twitter'); ?>">View Twitter Dashboard &raquo;</a>
		</p>
	</div>
	
	<div class="span12">
		<div id="overall_sentiment_stats" style="width: 95%; height: 500px;"></div>
		<p>
			<a class="btn" href="<?php echo url_for('/campaign/' . $currentCampaignId . '/realtime/sentiment'); ?>">View Twitter Sentiment Dashboard &raquo;</a>
		</p>
	</div>

	<div class="span12">
		<div id="overall_source_stats" style="width: 95%; height: 500px;"></div>
		<!--<p>
			<a class="btn" href="<?php echo url_for('/campaign/' . $currentCampaignId . '/realtime/sentiment'); ?>">View Twitter Sentiment Dashboard &raquo;</a>
		</p>-->
	</div>

<!-- Blocking FB Stats, as it does not render the page in a better way		
	<div class="span12">
		<h2>Facebook Stats</h2>
		<p> Coming soon! </p>
	</div><!--/span-->

</div>

<?php
end_content_for();


content_for('script');

// Since I know I want last 24 time series points, I might as well create them. 
$days = $db->run("select HOUR(posted_on) as day , posted_on from tweets where campaign_id = :cid and posted_on > DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 22 HOUR) group by HOUR(posted_on) order by posted_on DESC limit 24;", array(":cid" => $currentCampaignId));

?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawSummaryChart);


  function drawSummaryChart() {
		var options = {
		  title: 'Last 24 Hour Tweet(s) Activity',
		  hAxis: {title: 'Time of Day' },
		  vAxis: {title: 'Tweet Count' },
		};
		
		var data = new google.visualization.DataTable();
		data.addColumn('datetime', 'Day Of Week');
		data.addColumn('number', 'Total Tweets');
		data.addColumn('number', 'Positive Tweets');
		data.addColumn('number', 'Negative Tweets');

		<?php
		
			if(count($days) == 0) {
			//if(true) {	// Reverting to low-level reporting
				// Use low-level reporting when we have no proper dataset to work with
				$days = array();
				for($i = 23; $i >= 0; $i--) {
					$day = array();
					$day['day'] = $i;
					$day['posted_on'] = date('Y-m-d H:i:s', (round(strtotime("- $i hour") / 3600) * 3600));
				
					$days[] = $day;
				}
				
				echo "options.title = 'Current Tweet(s) Activity';";
			}
			
			foreach($days as $day) {
				$dailycount = $db->run("select count(*) as count from tweets where campaign_id = :cid and HOUR(posted_on) = :day  and posted_on > DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 22 HOUR) ", array(":day" => $day['day'], ":cid" => $currentCampaignId));
				$dailycount = $dailycount[0];
				$dailycount = $dailycount['count'];
				
				$positivecount = $db->run("select count(*) as count from tweet_mood where mood = :mood and tweets_id IN (select id from tweets where HOUR(posted_on) = :day and campaign_id = :cid and  posted_on > DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 22 HOUR) )", array(":mood" => "positive", ":day" => $day['day'], ":cid" => $currentCampaignId));
				$positivecount = $positivecount[0];
				$positivecount = $positivecount['count'];
				
				$negativecount = $db->run("select count(*) as count from tweet_mood where mood = :mood and tweets_id IN (select id from tweets where HOUR(posted_on) = :day and campaign_id = :cid and  posted_on > DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 22 HOUR) )", array(":mood" => "negative", ":day" => $day['day'], ":cid" => $currentCampaignId));
				$negativecount = $negativecount[0];
				$negativecount = $negativecount['count'];
				
				echo "data.addRow([new Date('" . $day['posted_on'] . "'), $dailycount, $positivecount, $negativecount]); \n\t";
			}
		?>
		
    var summarychart = new google.visualization.AreaChart(document.getElementById('overall_twitter_stats'));
    summarychart.draw(data, options);
    
    sentimentPieChart();	// Draw the Piechart after drawing the line chart
  }
  
  function sentimentPieChart() {
		var options = {
		  title: 'Overall Market Reaction',
		  legend: {
		  	position: 'bottom'
		  }
		};
		
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Mood');
		data.addColumn('number', 'Total');

		<?php
			$neutralcount = $db->run("select count(*) as count from tweet_mood where mood = :mood and tweets_id IN (select id from tweets where campaign_id = :cid)", array(":mood" => "neutral", ":cid" => $currentCampaignId));
			$neutralcount = $neutralcount[0];
			$neutralcount = $neutralcount['count'];
			echo "data.addRow(['Neutral ($neutralcount)', $neutralcount]); \n\t";
			
			$positivecount = $db->run("select count(*) as count from tweet_mood where mood = :mood and tweets_id IN (select id from tweets where campaign_id = :cid)", array(":mood" => "positive", ":cid" => $currentCampaignId));
			$positivecount = $positivecount[0];
			$positivecount = $positivecount['count'];
			echo "data.addRow(['Positive ($positivecount)', $positivecount]); \n\t";
			
			$negativecount = $db->run("select count(*) as count from tweet_mood where mood = :mood and tweets_id IN (select id from tweets where campaign_id = :cid)", array(":mood" => "negative", ":cid" => $currentCampaignId));
			$negativecount = $negativecount[0];
			$negativecount = $negativecount['count'];
			
			echo "data.addRow(['Negative ($negativecount)', $negativecount]); \n\t";
		?>
		
    var sentimentchart = new google.visualization.PieChart(document.getElementById('overall_sentiment_stats'));
    sentimentchart.draw(data, options);
    
    usageSourcePieChart();	// Now build the 
  }

  function usageSourcePieChart() {
		var options = {
		  title: 'Source of Tweets',
		  legend: {
		  	position: 'bottom'
		  }
		};
		
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Source');
		data.addColumn('number', 'Total');

		<?php
			$sources = $db->run("select count(*) as count, source from tweets where campaign_id = :cid group by source order by count desc limit 15", array(":cid" => $currentCampaignId));
			
			foreach($sources as $s) 
				echo "data.addRow(['" .preg_replace("/<a(.+)>(.+)<\/a>/", "\\2", $s['source']) . "'," . $s['count'] . "]); \n\t";
		?>
		
    var sentimentchart = new google.visualization.PieChart(document.getElementById('overall_source_stats'));
    sentimentchart.draw(data, options);
  }

</script>
<?php
end_content_for('script');

