<?php
content_for('body');

?>
<div class="row-fluid">
	<div class="span12">
		<h2>People Influencing your conversations</h2>

		<div id="cytoscapeweb" style="width:100%;height:500px;"></div>
	</div>
</div>

<?php
end_content_for('body');

content_for('script');

$kis = $db->run("select count(*) as count, twitter_users.screen_name as screen_name, twitter_users.profile_image_url, twitter_users.twitter_id from tweets, twitter_users where twitter_users.id = tweets.twitter_users_id and tweets.campaign_id = :cid group by twitter_users.id order by count desc limit 25", array(":cid" => $currentCampaignId));

$kis_count = 0;
foreach($kis as $ki) $kis_count += $ki['count'];
?>


    <script type="text/javascript" src="<?php echo url_for('/static/js/AC_OETags.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo url_for('/static/js/json2.min.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo url_for('/static/js/cytoscapeweb.min.js'); ?>"></script>

		<script type="text/javascript">
    // network data could alternatively be grabbed via ajax
    var options = {
		  // where you have the Cytoscape Web SWF
		  swfPath: "<?php echo url_for('/static/swf/CytoscapeWeb'); ?>",
		  // where you have the Flash installer SWF
		  flashInstallerPath: "<?php echo url_for('/static/swf/playerProductInstall'); ?>"
		};
		
		var network = {
			dataSchema: {	// Defining the Network Schema Definition
				nodes: [
					// id refers to twitter_id anyways
					{ name: "label", type: "string", defValue: "_ashwanthkumar" },	// Twitter Handle
					{	name:	"handle", type: "string", defValue: "_ashwanthkumar" },
					{ name: "image", type: "string", defValue: "http://gravatar.com/avatar/1234567890?d=mm" },	// Profile Image URL
					{	name: "contribution", type: "number", defValue: 1 },	// Contribution made to the Campaign
					{	name: "size", type: "number", defValue: 25 },
					{	name: "x", type: "number" },
					{	name: "y", type: "number" }
				],
				edges: [
					{ name: "weight", type: "number", defValue: 1},
					{	name: "size", type: "number", defValue: 25 }
				],
			},	// End of Schema Definition
			
			data: {
				nodes: [
					{ id: "campaign_id", label: "<?php echo $campaignName; ?>", image: "http://gravatar.com/avatar/1234567890?d=identicon", size: <?php echo ($kis_count / count($kis)) + 20; ?> },	// Add the Campaign Id Node
					<?php
						foreach($kis as $ki) {
							echo "{ id: '" . $ki['twitter_id'] . "', label: '@" . $ki['screen_name'] . "(" . $ki['count'] . ")', handle: '" . $ki['screen_name'] . "', image: '" . $ki['profile_image_url'] . "', contribution: " . $ki['count']  . ", size: " . ((($ki['count'] / $kis_count) * 100) + 25) . ",x: Math.random(), y: Math.random() },\n\t";
						}
					?>
				], 
				edges: [
					<?php
						foreach($kis as $ki) {
							echo "{ weight: " . $ki['count'] . ", source: '" . $ki['twitter_id'] . "', target: 'campaign_id', size: " . ($kis_count - $ki['count'])  ." },\n\t";
						}
					?>
				]
			}
		};
		
		var style = {
			nodes: {
				image: { passthroughMapper: { attrName: "image" } },
				label: { passthroughMapper: { attrName: "label" } }, 
				size: { passthroughMapper: { attrName: "size" } }, 
				labelVerticalAnchor: "top",
			}, 
			edges: {
				size: { passthroughMapper: { attrName: "size" } },
			}
		};

		var vis = new org.cytoscapeweb.Visualization("cytoscapeweb", options);

		vis.draw({
			visualStyle: style,
			network: network 
		});

		vis.addListener("select", "nodes", function(evt) {
    	var nodes = evt.target;
	    var node = nodes[0];
	    
	    if(node.data.id != "campaign_id") window.open("http://twitter.com/" + node.data.handle, node.data.handle);
		});
		
		// TODO: Try to implement this based on an AJAX call and update the things frequently
		// setTimeout(window.location.href = window.location.href, 60000);	// Reload every 60 secs. 
	</script>
<?php
	end_content_for('script');

