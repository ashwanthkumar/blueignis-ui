<?php

/**
 *	Starting point of the BlueIgnis Web Application
 *
 *	@author	Ashwanth Kumar
 *	@date	05/04/2012
 */

// Including the required libraries
require_once(dirname(__FILE__) . "/lib/limonade.php");

/**
 *	Configure application wide settings here. All Limonade initialization, 
 *	Database connection parameters, common layout settings, everything 
 *	can be set here. 
 *
 *	Before Limonade calls this function it calls its default configure()
 *	implementation which sets the default values for the application. 
 */
function configure() {
	// Including the general configuration values from our old configuration
	include_once("config.php");
}

/**
 *	Method called before every request is being processed by the limonade. 
 *
 *	You can put in certain parts of code that are to be performed on every 
 *	page request. Instead of re-writing the same code over and over again 
 *	in each request handler, it is a good practice to put them here. 
 */
function before() {
	global $db;
	if(!isset($_SESSION['user']) && isset($_SESSION['authId'])) {
		$userId = $_SESSION['authId'];
		$user = $db->select("user", "id = :uid", array(":uid" => $userId));
		// TODO: No checking as we are assuming user shall exist
		$user = $user[0];

		$_SESSION['user'] = $user;
	}
}

/**
 *	Route(s) Section
 */
dispatch("/", 'blueignis');
dispatch("/login", 'login');
dispatch_post("/authenticate", 'authenticate');
dispatch("/logout", 'logout');

dispatch("^/campaign/(\d+)", 'campaignDashboard');
dispatch("^/campaign/(\d+)/realtime/twitter", 'campaignRealtimeTwitterDashboard', array('id'));
dispatch("^/campaign/(\d+)/realtime/sentiment", 'campaignRealtimeSentimentDashboard', array('id'));
dispatch("^/campaign/(\d+)/report/ki", 'campaignReportKeyInfluencers', array("id"));

dispatch("^/campaign/(\d+)/delete", "deleteCampaign", array("id"));
 
dispatch("/campaign/create", 'createNewCampaign');
dispatch_post("/campaign/create/run", 'runCampaign');

dispatch("/user/profile", 'viewUserProfile');

/**
 *	End of Route(s) Section
 */

/**
 *	Start the Web Application
 */
run();

