-- Given table tweets with textual textumn text


CREATE TABLE counter (

    id int unsigned NOT NULL AUTO_INCREMENT,

    primary key (id)

);

----

CREATE PROCEDURE populate_counter()

BEGIN

    SET @i = 0;

    SET @n = (select max(LENGTH(text) - LENGTH(REPLACE(text, ' ', ''))+1) from tweets);

    WHILE @i<@n DO

        INSERT INTO counter (id) VALUES (NULL);

        SET @i = @i + 1;

    END WHILE;

END

----

call populate_counter()

----

select 

	word, 

	count(*) as freq

from (

	select 

		case 

			when id <= LENGTH(text) - LENGTH(REPLACE(text, ' ', ''))+1 then SUBSTRING_INDEX(SUBSTRING_INDEX(text, ' ', id), ' ', -1) 

			else '' 

		end as word 

	from tweets, counter

) t

where 

	0 < LENGTH(word)

group by word;

