<?php

require_once(__DIR__ . "/lib/class.db.php");
require_once(__DIR__ . "/lib/predis/Autoloader.php");

/**
 *	Database related configurations
 */
$db_name = "fohubcom_blueignis";
// $db_user = "fohubcom_blueignis";
$db_user = "root";

$db_host = "localhost";
// $db_pass = "BlueIgnis@AshwanthKumar.inLabs";
$db_pass = "root";

global $db;
$db = new db("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);

/**
 *	Redis related parameters
 */
global $predis;
Predis\Autoloader::register();
$predis = new Predis\Client();	// Connect to local system at 127.0.0.1 at port 6379

date_default_timezone_set("Asia/Kolkata");

session_start();

